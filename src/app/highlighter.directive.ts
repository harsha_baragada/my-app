import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHighlighter]',
})
export class HighlighterDirective {
  constructor(private element: ElementRef) {
    element.nativeElement.style.backgroundColor = 'yellow';
  }
}
