import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
} from '@angular/core';

@Component({
  selector: 'app-interact',
  templateUrl: './interact.component.html',
  styleUrls: ['./interact.component.css'],
})
export class InteractComponent
  implements
    DoCheck,
    OnInit,
    OnChanges,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked {
  @Input() userData: any;
  @Output() buttonClicked = new EventEmitter<any>();
  name: any;
  isNameValid: boolean;
  constructor() {
    console.log('constructor');
  }

  ngOnInit(): void {
    console.log('ngOnInIt');
  }

  ngDoCheck() {
    console.log('ngDoCheck');
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('ngOnchanges ' + changes);
  }

  ngAfterContentInit() {
    console.log('ngAfterContentInIt');
  }

  ngAfterContentChecked() {
    console.log('ngAfterContentChecked');
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit');
  }

  ngAfterViewChecked() {
    console.log('ngAfterViewChecked');
  }

  clickButton() {
    this.buttonClicked.emit(this.userData.name);
  }
  validateName() {
    this.isNameValid = false;
    if (this.name) {
      alert('success');
    } else {
      this.isNameValid = true;
    }
  }
}
