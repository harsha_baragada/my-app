import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'squareroot',
})
export class SquareRootPipe implements PipeTransform {
  transform(input: number): number {
    return Math.sqrt(input);
  }
}
