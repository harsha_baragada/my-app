import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
  constructor(private http: HttpClient) {}

  getPosts(): Observable<any> {
    console.log('service');
    return this.http.get('http://localhost:8102/dealstoday/home');
  }

  saveUser(user: any): Observable<any> {
    let url = 'http://localhost:5000/skicco/userSignup';
    let body = JSON.stringify(user);
    return this.http.post(url, body, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      observe: 'response',
    });
  }
}
