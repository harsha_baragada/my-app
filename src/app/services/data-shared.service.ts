import { Injectable } from '@angular/core';

@Injectable()
export class DataSharedService {
  data: any;
  constructor() {}

  setData(inputData: any) {
    this.data = inputData;
  }

  getData() {
    return this.data;
  }
}
