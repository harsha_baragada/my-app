import { DataSharedService } from './../services/data-shared.service';
import { AppService } from './../services/app.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  title: any = '200';
  today: Date = new Date();
  posts: any[] = [];
  value: number = 2;
  users: any;
  user: User = {
    emailId: undefined,
    pwd: undefined,
  };
  dataParent: any = [
    {
      name: 'peter',
      id: 1001,
      department: 'ENT',
    },
    {
      name: 'Emile',
      id: 1002,
      department: 'General',
    },
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private _service: AppService,
    private sharedService: DataSharedService
  ) {
    localStorage.setItem('item', 'profile');
    this.user.emailId = 'daduk@givmail.com';
    this.user.pwd = '123456Asdf@';
  }

  ngOnInit(): void {
    console.log('ngOnInIt');
    /*  this._service.getPosts().subscribe((posts) => {
      console.log(posts);
      this.posts = posts.responseMsg.cashbackDeals;
    });
    this._service.saveUser(this.user).subscribe((response) => {
      console.log(response);
    });*/
    this.users = this.sharedService.getData();
  }

  buttonClickesdFromChild(value) {
    alert(value);
  }
}

export interface User {
  emailId: string;
  pwd: string;
}
