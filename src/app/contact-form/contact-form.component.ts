import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css'],
})
export class ContactFormComponent implements OnInit {
  contactForm: FormGroup;
  nameError: string;
  emailError: string;
  mobileNumberError: string;
  emailFormatError: string;
  mobilrNumberFormatError: string;
  NUMBER_FORMAT = '^((\\+91-?)|0)?[0-9]{10}$';
  itShouldBeANumber: string;
  constructor() {
    this.nameError = 'name is required';
    this.emailError = 'email is required';
    this.mobileNumberError = 'mobile number is required';
    this.emailFormatError = 'The email you entered is not valid';
    this.mobilrNumberFormatError =
      'The mobile number you have entered is not valid';
    this.itShouldBeANumber = 'Mobile number should be a number';
  }

  ngOnInit(): void {
    this.contactForm = this.createContactForm();
  }

  createContactForm() {
    return new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email]),
      mobile: new FormControl('', [
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(this.NUMBER_FORMAT),
      ]),
    });
  }

  onSubmit() {
    console.log(this.contactForm);

    console.log('the email is : ' + this.contactForm.value.email);
  }

  isMobileNumberLengthValid(): boolean {
    if (
      !this.contactForm.controls.mobile.errors.required &&
      this.contactForm.controls.mobile.errors.minLength
    ) {
      return (
        this.contactForm.controls.mobile.errors.minLength.actualLength ==
        this.contactForm.controls.mobile.errors.minLength.requiredLength
      );
    }
    return false;
  }
  isMobileNumberValid(): boolean {
    if (
      !this.contactForm.controls.mobile.errors.required &&
      !this.contactForm.controls.mobile.errors.minLength &&
      this.contactForm.controls.mobile.errors.pattern
    ) {
      return true;
    }

    return false;
  }
}
