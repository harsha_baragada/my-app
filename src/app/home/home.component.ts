import { DataSharedService } from './../services/data-shared.service';
import { AppService } from './../services/app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title = 'car';
  show: boolean;
  names: string[] = ['peter', 'sam', 'Eva', 'bob'];
  users: any[] = [];
  deals: any[] = [];
  customer: Customer = {
    id: 1001,
    account: 'bonus',
    name: 'olive',
    addresses: [
      {
        addressType: 'primary',
        doorNo: 655,
        street: 'bell street',
        city: 'newyork',
        country: 'USA',
      },
      {
        addressType: 'secondary',
        doorNo: 5889,
        street: 'no street',
        city: 'newyork',
        country: 'USA',
      },
      ,
    ],
  };
  constructor(
    private route: Router,
    private _service: AppService,
    private sharedService: DataSharedService
  ) {
    localStorage.setItem('item', 'Home');
    console.log('constructor');
    this.users = [
      {
        name: 'peter',
        id: 1001,
        email: 'peter@mail.com',
      },
      {
        name: 'sam',
        id: 1002,
        email: 'sam@mail.com',
      },
      {
        name: 'eva',
        id: 1003,
        email: 'eva@mail.com',
      },
      {
        name: 'bob',
        id: 1004,
        email: 'bob@mail.com',
      },
    ];
  }
  ngOnInit() {
   /*  this._service.getPosts().subscribe((posts) => {
      console.log(posts);
      this.deals = posts.responseMsg.cashbackDeals;
    }); */

    this.sharedService.setData(this.users);
  }

  isLong() {
    if (this.title.length > 6) this.show = true;
    else this.show = false;
  }

  takeMeToProfile() {
    if (this.customer.addresses.length > 1) {
      this.route.navigate(['/profile']);
    } else {
      alert('user is not having sufficient addresses');
    }
  }
}

export interface Customer {
  id: number;
  name: string;
  account: string;
  addresses: Address[];
}

export interface Address {
  addressType: string;
  doorNo: number;
  street: string;
  city: string;
  country: string;
}
